# O co chodzi?
Program w klasie Main ma wygenerować talię 52 kart. Następnie wygenerowaną talię potasować,
czyli zmienić kolejność kart. Na końcu, co najważniejsze, program ma rozdać karty na 3 kupki po 13 kart.
# Jak sprawdzić poprawność rozwiązania?
W projekcie istnieją testy integracyjne. Dopisz logikę tak, by testy były "zielone"
# Zasady
 - Testy nie mogą się zmienić.
 - Postaraj się nie używać dostępnego API. Jeżeli, na przykład, trzeba wykonać operację na kolekcji - zaimplementuj ją (tę operację) własnoręcznie.
# Jak oddać rozwiązanie?
Zrób forka tego repozytorium, aby rozwiązać zadanie, a następnie Merge Request do tego repozytorum.
