package pl.adriankozlowski;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        Collection<Card> cards = main.generateDeck();
        cards = main.shuffle(cards);
        Map<String, List<Card>> distributedCards = main.dealCards(cards);
    }

    public Collection<Card> shuffle(Collection<Card> cards) {
        return null;
    }

    public Map<String, List<Card>> dealCards(Collection<Card> cards) {
        return null;
    }

    public Collection<Card> generateDeck() {
        return null;
    }
}
